@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-3">Profile pix</div>
        <div class="col-md-9">Profile Info</div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8">Groups(4)</div>
        <div class="col-md-4">New Group</div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-3">Group 1</div>
        <div class="col-md-3">Group 2</div>
        <div class="col-md-3">Group 3</div>
        <div class="col-md-3">Group 4</div>
    </div>
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">Calendar</div>
    </div>
</div>


@endsection
