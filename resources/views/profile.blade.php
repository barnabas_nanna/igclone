@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-3">
            <img src="{{ $user->profile->profileImage() }}" class="w-100"/>
            @can('update', $user->profile)
            <a href="{{route('profile.edit', [$user->id])}}">Edit Profile</a>
           @endcan
        </div>

        <div class="col-md-9">
            <div class="d-flex justify-content-between align-items-baseline">

                <div class="d-flex align-items-center pb-3">
                    <div class="h4 ">{{ $user->username }}</div>
                    <button class="btn btn-primary ml-4">Follow</button>
                </div>

                @can('update', $user->profile)
                    <a href="{{route('posts.create')}}">New Post</a>
                @endcan
            </div>

            <div class="d-flex">
                <div class="pr-5"><strong>{{$postCount}}</strong> posts</div>
                <div class="pr-5"><strong>{{$followersCount}}</strong> posts</div>
                <div class="pr-5"><strong>{{$followingCount}}</strong> posts</div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8">Groups(4)</div>
        <div class="col-md-4">New Group</div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-3">Group 1</div>
        <div class="col-md-3">Group 2</div>
        <div class="col-md-3">Group 3</div>
        <div class="col-md-3">Group 4</div>
    </div>

</div>

<div class="container">
    <div class="row">
            @foreach ($user->posts as $post)
                <div class="col-4 pt-4">
                    <a href="/p/{{ $post->id }}">
                        <img src="/storage/{{ $post->image }}" class="w-100"/>
                    </a>
                </div>
            @endforeach
    </div>
</div>


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">Calendar</div>
    </div>
</div>


@endsection
