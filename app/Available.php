<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Available extends Model
{
    public function user(){
        return $this->belongsTo(User::class);
    }
}
