<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function tags(){
        return $this->morphToMany(Tag::class,'taggable');
    }

    public function image(){
       return $this->morphOne(Images::class, 'imageable');
    }

    public function comments(){
        return $this->morphMany(Comment::class, 'commentable');
    }
}
