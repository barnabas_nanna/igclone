<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/email', function (){
    return new \App\Mail\NewUserWelcomeMail();
});

Route::view('/welcome', 'welcome');

Route::get('foo', function () {
    return 'Hello World';
});


Route::get('/user', 'UserController@index');

Route::get('user/{id}', function ($id) {
    return 'User '.$id;
});

Route::get('posts/{post}/comments/{comment}', function ($postId, $commentId) {
    echo $postId.$commentId;
});

Route::prefix('admin')->group(function () {
    Route::get('users', function () {
        // Matches The "/admin/users" URL
        return 'fgfg';
    });
});

Auth::routes();

//APPLICATION ROUTES
Route::get('/profile/create', 'ProfileController@create')->name('profile.create');
Route::get('/profile/{user}', 'ProfileController@index')->name('profile.show');
Route::get('/profile/{user}/edit', 'ProfileController@edit')->name('profile.edit');
Route::patch('/profile/{user}', 'ProfileController@update')->name('profile.update');



Route::get('/', 'PostController@index')->name('posts.index');
Route::get('/p/create', 'PostController@create')->name('posts.create');
Route::post('/p', 'PostController@store')->name('posts.store');
Route::get('/p/{post}', 'PostController@show')->name('posts.show');
